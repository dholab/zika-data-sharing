#!/usr/bin/python

# This script targets the client api version 0.4.0 and later
import labkey
import json
import datetime
#import dateutil
import plotly.plotly as py
import plotly.graph_objs as go
import argparse
import sys

parser = argparse.ArgumentParser()
parser.add_argument("-animal_ids", help="Specify a comma separated list of animal ids\nE.g. -animal_ids 393422,826226,912116", type=str, required=True)
parser.add_argument("-fluids", help="Specify a comma separated list of fluid types to plot\nE.g. -fluids plasma,pan_urine,CSF\nfluid names need to match values in database.", type=str,default="4")
args = parser.parse_args()

server_context = labkey.utils.create_server_context('dholk.primate.wisc.edu', 'dho/public/Zika/public/ZIKV-001-public', use_ssl=True)

# extract all plasma viral loads

my_results = labkey.query.select_rows(
    server_context=server_context,
    schema_name='study',
    query_name='viral_loads'
)

# set challenge date to use for each animal
challenge_date = {
'393422' : datetime.date(2016, 2, 15),
'826226' : datetime.date(2016, 2, 15),
'912116' : datetime.date(2016, 2, 15)
}
animalIdColors = {
'393422' : 'rgb(102, 194, 165)',
'826226' : 'rgb(252, 141, 98)',
'912116' : 'rgb(141, 160, 203)'
}

#animalIds = ['393422','826226','912116']
animalIds = args.animal_ids.split(',')
for anid in animalIds:
	if len(anid) == 0:
		print("Incorrect comma separated list of animal ids",file=sys.stderr)
		print("script saw: " + args.animal_ids,file=sys.stderr)
		sys.exit()
fluidTypes = args.fluids.split(',')

for ft in fluidTypes:
	if len(ft) == 0:
		print("Incorrect comma separated list of fluid types",file=sys.stderr)
		print("script saw: " + args.fluids,file=sys.stderr)
		sys.exit()
	
#fluidTypes = ['plasma','pan_urine','oral swab','CSF']
viralLoadsMaster = {}

for animal_id in animalIds:
	viralLoadsMaster[animal_id] = {}
	
	for fluidType in fluidTypes:
		trace_x = []
		trace_y = []
		viralLoadsMaster[animal_id][fluidType] = [trace_x,trace_y]

maxDaysPost = 0;
# iterate over LabKey results
for resultRow in my_results['rows']:
	# get animal id
	animalId = resultRow['ParticipantId']
	
	# get test fluid
	fluid = resultRow['testid']
	
	# get viral load - if 0, change to 1 to enable proper plotting with log scale y-axis
	if (resultRow['result']) == 0:
		viralLoad = '1.0'
	else:
		viralLoad = resultRow['result']
	
	# calculate days post-infection for each participant
	viral_load_date = datetime.datetime.strptime(resultRow['date'], "%Y/%m/%d %H:%M:%S").date()
	days_post_infection = str((viral_load_date - challenge_date[animalId]).days)
	
	#determine x scale days since infection max value
	daysPostCurrent = int(days_post_infection)
	if daysPostCurrent > maxDaysPost:
		maxDaysPost = daysPostCurrent
	
	if fluid in fluidTypes:
		viralLoadsMaster[animalId][fluid][0].append(days_post_infection)
		viralLoadsMaster[animalId][fluid][1].append(viralLoad)

for fluid in fluidTypes:
	fluidData = []
	
	for animalId in viralLoadsMaster:
		
		fluid_trace = go.Scatter( x = viralLoadsMaster[animalId][fluid][0],y = viralLoadsMaster[animalId][fluid][1], name = animalId,
			line = dict( color = (animalIdColors[animalId]), width = 4))
		fluidData.append(fluid_trace)

	fluid_layout = go.Layout(
	    title=fluid + ' Viral Load',
	    titlefont = dict(
		family='Arial, sans-serif',
		size=24,
		color='black'
		),   	
	    xaxis=dict(
		range=[0, maxDaysPost + 0.1],
		title='Days since Zika virus infection',
		titlefont=dict(
		    family='Arial, sans-serif',
		    size=24,
		    color='black'
		)
	    ),
	    yaxis=dict(
		type='log',
		exponentformat='power',
		range=[-.1, 7],
		title='Viral RNA copies per mL',
		titlefont=dict(
		    family='Arial, sans-serif',
		    size=24,
		    color='black'
		)
	    )
	)
	
	# make graphs
	fluid_fig = go.Figure(data=fluidData, layout=fluid_layout)
	plot_url = py.plot(fluid_fig, filename='ZIKV-001: ' + fluid + ' Viremia')

	#urine_fig = go.Figure(data=urine_data, layout=urine_layout)
	#plot_url = py.plot(urine_fig, filename='ZIKV-001: Urine viremia')




